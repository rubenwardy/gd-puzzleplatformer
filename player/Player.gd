extends KinematicBody2D

const speed = 200
const jumpForce = 200
const gravity = 800

var velocity: Vector2 = Vector2()
var hasDoubleJumped = false
var wallStick = -1
var wallDirection = 0
var pulling: RigidBody2D = null
var pullingOffset = Vector2()

onready var sprite = $Sprite
onready var interactZone = $InteractZone

signal respawned

var world: Node

func get_wall_direction():
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		var dot = collision.normal.dot(Vector2(1, 0))
		if dot == 1 or dot == -1:
			return dot
	return 0


func update_input(delta):
	var on_wall = is_on_wall() or wallStick > 0
	var friction = 0.6 if is_on_floor() or on_wall else 0.1
	if wallStick > 0:
		wallStick -= delta
	if not on_wall:
		wallStick = -1

	var walk = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	if wallStick <= 0:
		if pulling:
			walk *= 0.25
		velocity.x += (speed * walk - velocity.x) * friction

	if Input.is_action_just_pressed("jump") and pulling == null:
		if on_wall and wallDirection != 0:
			wallStick = -1
			if wallDirection != 0:
				velocity.x = speed * wallDirection
				velocity.y = min(-jumpForce, velocity.y)
				hasDoubleJumped = false
		elif is_on_floor():
			velocity.y -= jumpForce
			hasDoubleJumped = false
		elif not hasDoubleJumped:
			hasDoubleJumped = true
			velocity.y = min(-jumpForce * 0.8, velocity.y)


func _physics_process(delta):
	update_input(delta)
	velocity = move_and_slide(velocity, Vector2.UP, false, 4, 0.785398, false)
	velocity.y += gravity * delta

	if abs(velocity.x) > 0.001:
		sprite.playing = true
		if pulling:
			set_facedir(pulling.global_position.x > global_position.x)
		else:
			set_facedir(velocity.x > 0)

	else:
		sprite.playing = false
		sprite.frame = 0

	for i in get_slide_count():
		var collision = get_slide_collision(i)
		var dot = collision.normal.dot(Vector2(1, 0))

		# Stick to walls for 60ms (makes wall jumping easier)
		if (dot == 1 or dot == -1) and wallStick == -1:
			wallStick = 0.06
			wallDirection = dot

		# Push rigid bodies
		if collision.collider is RigidBody2D:
			var body = collision.collider as RigidBody2D
			body.apply_central_impulse(-collision.normal * velocity.length() * 1)

	# Respawn
	if position.y > 200:
		emit_signal("respawned")


func set_facedir(is_right):
	sprite.flip_h = is_right
	interactZone.scale.x = -1 if is_right else 1

func _unhandled_input(event):
	if event.is_action_released("interact"):
		if pulling and is_instance_valid(pulling):
			pulling.detach()
			pulling = null
			return

		var areas = interactZone.get_overlapping_areas()
		for area in areas:
			if area.has_method("interact"):
				area.interact(self)

		var bodies = interactZone.get_overlapping_bodies()
		for body in bodies:
			if body.has_method("attach"):
				body.attach(self)
				pulling = body


func damage(hitter, points):
	world.blood_splatter(position, velocity)
	emit_signal("respawned")


func _on_respawned():
	pulling = null
	position.x = 0
	position.y = 0
