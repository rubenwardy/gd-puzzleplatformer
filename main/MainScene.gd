extends Node2D

onready var player = $Player
onready var levelUI = $HUD/LevelFinished

export(Array, PackedScene) var levels

var currentLevelId = 0
var currentLevel: Node

func _ready():
	load_level(0)
	levelUI.hide()

func load_level(id: int) -> void:
	currentLevelId = id

	var level = levels[id]
	if currentLevel:
		currentLevel.queue_free()

	var node = level.instance()
	node.connect("level_completed", self, "_on_level_completed")

	add_child(node)
	currentLevel = node

	player.world = node
	player.position = Vector2(0, -10)


func _on_level_completed():
	levelUI.show()
	$HUD/LevelFinished/VBoxContainer/Next.grab_focus()


func _on_next_level_pressed():
	levelUI.hide()
	var id = currentLevelId + 1
	if id < levels.size():
		load_level(id)
	else:
		var tree = get_tree()
		tree.paused = false
		tree.change_scene("res://mainmenu/MainMenu.tscn")
