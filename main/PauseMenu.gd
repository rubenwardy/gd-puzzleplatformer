extends ColorRect

func _process(delta):
	var tree = get_tree()
	visible = tree.paused
	if Input.is_action_just_released("pause"):
		tree.paused = !tree.paused
		if tree.paused:
			$VBoxContainer/Continue.grab_focus()

func _on_continue():
	get_tree().paused = false
	hide()

func _on_exit():
	var tree = get_tree()
	tree.paused = false
	tree.change_scene("res://mainmenu/MainMenu.tscn")

func _on_exit_to_os():
	get_tree().quit()
