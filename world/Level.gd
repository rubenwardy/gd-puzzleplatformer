class_name Level
extends Node

onready var bloodParticles = $BloodParticles
onready var boxParticles = $BoxParticles
onready var tilemap = $TileMap

signal level_completed

var Player = preload("../player/Player.gd")

func blood_splatter(pos: Vector2, vel: Vector2):
	bloodParticles.position = pos
	bloodParticles.restart()

func box_explosion(pos: Vector2):
	boxParticles.position = pos
	boxParticles.restart()

func _on_goal_body_entered(body):
	if body is Player:
		$Goal/GoalParticles.emitting = true
		emit_signal("level_completed")
