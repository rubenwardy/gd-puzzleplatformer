extends RigidBody2D

onready var spawnPosition = position
var shouldReset = false

var attachmentParent: PhysicsBody2D = null
var attachmentOffset = Vector2()

func damage(hitter, points):
	get_parent().box_explosion(position)
	shouldReset = true

func _integrate_forces(state):
	if shouldReset:
		state.transform = Transform2D(0.0, spawnPosition)
		state.linear_velocity = Vector2()
		shouldReset = false
		detach()
	elif attachmentParent:
		var target = attachmentParent.global_position + attachmentOffset
		state.linear_velocity.x = (target.x - global_position.x) / state.step

func attach(parent: PhysicsBody2D):
	parent.connect("respawned", self, "detach")
	attachmentParent = parent
	attachmentOffset = (global_position - parent.global_position) * 1.1
	weight *= 2
	sleeping = false
	can_sleep = false

func detach():
	if attachmentParent:
		attachmentParent.disconnect("respawned", self, "detach")
	attachmentParent = null
	weight /= 2
	sleeping = false
	can_sleep = true
