extends TileSet
tool


func _is_tile_bound(drawn_id, neighbour_id):
	var leaves = find_tile_by_name("Leaves")
	var trunk = find_tile_by_name("Trunk")
	#return neighbour_id in get_tiles_ids()
	return (neighbour_id == trunk and drawn_id == leaves) or \
		(neighbour_id == leaves and drawn_id == trunk)
