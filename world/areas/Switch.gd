extends Area2D

const SWITCH_LEFT = 6
const SWITCH_RIGHT = 7

onready var switchSound = $SwitchSound

export var is_inverted = false

signal state_changed(pressed)

func _ready():
	call_deferred("emit_signal", "state_changed", is_inverted)

func interact(player: Node):
	var tilemap = get_parent().tilemap
	var tilePos = tilemap.world_to_map(position.round())
	var tile = tilemap.get_cellv(tilePos)
	switchSound.play()

	var newTile = SWITCH_LEFT if tile == SWITCH_RIGHT else SWITCH_RIGHT
	tilemap.set_cellv(tilePos, newTile)
	print("Switching!")
	emit_signal("state_changed", (newTile == SWITCH_RIGHT) != is_inverted)
