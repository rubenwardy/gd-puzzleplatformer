class_name DamageArea
extends Area2D

export(int) var hitpoints = 10000

func _on_body_entered(body):
	if body.has_method("damage"):
		body.damage(self, hitpoints)
