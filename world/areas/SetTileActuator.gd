extends Node

enum Operator {AND, OR}

export(Array, NodePath) var inputs
export(Operator) var operator = Operator.AND

export(Array, Vector2) var target_positions
export(int) var tile_one = 12
export(int) var tile_two = -1

var states = []

func _ready():
	for i in len(inputs):
		var node = get_node(inputs[i])
		node.connect("state_changed", self, "_on_signal", [ i ])

	states.resize(len(inputs))

func update():
	var pressed = true
	if operator == Operator.AND:
		for state in states:
			pressed = pressed and state
	elif operator == Operator.OR:
		pressed = false
		for state in states:
			if state:
				pressed = true
				break
	else:
		assert(false, "Unknown operator")

	var tilemap = get_parent().tilemap
	var newTargetTile = tile_two if pressed else tile_one
	for target in target_positions:
		tilemap.set_cellv(target, newTargetTile)

func _on_signal(pressed, source):
	states[source] = pressed
	update()
