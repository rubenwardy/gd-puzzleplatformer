extends Area2D

const BUTTON_OFF = 17
const BUTTON_ON = 18

onready var switchSound = $SwitchSound

export var is_inverted = false

signal state_changed(pressed)

func _ready():
	call_deferred("emit_signal", "state_changed", is_inverted)

func set_state(pressed):
	var tilemap = get_parent().tilemap
	var tilePos = tilemap.world_to_map(position.round())
	var tile = tilemap.get_cellv(tilePos)
	switchSound.play()

	tilemap.set_cellv(tilePos, BUTTON_ON if pressed else BUTTON_OFF)
	emit_signal("state_changed", pressed != is_inverted)

func _on_body_entered(body):
	set_state(get_overlapping_bodies().size() > 0)

func _on_body_exited(body):
	set_state(get_overlapping_bodies().size() > 0)
