extends ColorRect

func _ready():
	$MarginContainer/HBoxContainer/Left/Options/NewGame.grab_focus()

func _on_new_game():
	get_tree().change_scene("res://main/MainScene.tscn")

func _on_fullscreen():
	OS.window_fullscreen = !OS.window_fullscreen

func _on_exit():
	get_tree().quit()
